#pragma once 

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>
#include "opencv2/core/types.hpp"

#include <opencv2/aruco/charuco.hpp>
#include <opencv2/aruco/dictionary.hpp>

class CharucoGridCornerDetector : public cv::FeatureDetector
{
	
	/*
	    "{@outfile |<none> | Output image }"
        "{w        |       | Number of squares in X direction }"
        "{h        |       | Number of squares in Y direction }"
        "{sl       |       | Square side length (in pixels) }"
        "{ml       |       | Marker side length (in pixels) }"
        "{d        |       | dictionary: DICT_4X4_50=0, DICT_4X4_100=1, DICT_4X4_250=2,"
        "DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, DICT_5X5_250=6, DICT_5X5_1000=7, "
        "DICT_6X6_50=8, DICT_6X6_100=9, DICT_6X6_250=10, DICT_6X6_1000=11, DICT_7X7_50=12,"
        "DICT_7X7_100=13, DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = 16}"
        "{m        |       | Margins size (in pixels). Default is (squareLength-markerLength) }"
        "{bb       | 1     | Number of bits in marker borders }"
        "{si       | false | show generated image }";

	*/

	int m_borderBits = 1;
	int m_squaresX = 12;
	int m_squaresY = 9;
	float m_squareLength = 60;
	float m_markerLength = 45;
	int m_dictionaryId = 5;
	bool m_showRejected = true;
	bool m_refindStrategy = false;

	//TODO put it into configFile

	cv::Ptr<cv::aruco::Dictionary> m_dictionary = nullptr;

	cv::Ptr<cv::aruco::CharucoBoard> m_charucoboard = nullptr;
	cv::Ptr<cv::aruco::Board> m_board = nullptr;
	cv::Ptr<cv::aruco::DetectorParameters> m_detectorParams = nullptr;
	cv::Mat camMatrix;
	cv::Mat distCoeffs;

public:

	CharucoGridCornerDetector();
	~CharucoGridCornerDetector();

	virtual void detect(cv::InputArray image, CV_OUT std::vector<cv::KeyPoint>& keypoints,	cv::InputArray mask = cv::noArray()) override;

	void detectMat(cv::Mat &image, std::vector< cv::KeyPoint > &keypoints, const cv::Mat &mask = cv::Mat());

	
};

