#include "CharucoGridCornerDetecor.h"


CharucoGridCornerDetector::CharucoGridCornerDetector()
{
	m_detectorParams = cv::aruco::DetectorParameters::create();
	m_dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(m_dictionaryId));
	float axisLength = 0.5f * ((float)std::min(m_squaresX, m_squaresY) * (m_squareLength));
	// create charuco board object
	m_charucoboard =
		cv::aruco::CharucoBoard::create(m_squaresX, m_squaresY, m_squareLength, m_markerLength, m_dictionary);
		m_board = m_charucoboard.staticCast<cv::aruco::Board>();
}

CharucoGridCornerDetector::~CharucoGridCornerDetector()
{
}

void CharucoGridCornerDetector::detect(cv::InputArray image, CV_OUT std::vector<cv::KeyPoint>& keypoints, cv::InputArray mask)
{
	cv::Mat img = image.getMat();
	cv::Mat matMask = mask.getMat();
	this->detectMat(img, keypoints, matMask);
}

void CharucoGridCornerDetector::detectMat( cv::Mat &image, std::vector< cv::KeyPoint > &keypoints, const cv::Mat &mask)
{
	std::vector< int > markerIds, charucoIds;
	std::vector< std::vector< cv::Point2f > > markerCorners, rejectedMarkers;
	std::vector< cv::Point2f > charucoCorners;
	cv::Vec3d rvec, tvec;

	// detect markers
	cv::aruco::detectMarkers(image, m_dictionary, markerCorners, markerIds, m_detectorParams,
		rejectedMarkers);
	
	// interpolate charuco corners
	int interpolatedCorners = 0;
	if (markerIds.size() > 0)
		interpolatedCorners =
		cv::aruco::interpolateCornersCharuco(markerCorners, markerIds, image, m_charucoboard,
			charucoCorners, charucoIds, camMatrix, distCoeffs);

	cv::Mat imageCopy;
	// draw results
	image.copyTo(imageCopy);
	if (markerIds.size() > 0) {
		cv::aruco::drawDetectedMarkers(imageCopy, markerCorners);
	}

	
	if (interpolatedCorners > 0) {
		cv::Scalar color;
		color = cv::Scalar(255, 0, 255);
		cv::aruco::drawDetectedCornersCharuco(imageCopy, charucoCorners, charucoIds, color);
	}

	keypoints.clear();
	for (size_t i = 0; i < charucoCorners.size(); i++)
	{
		cv::KeyPoint kp;
		kp.class_id = charucoIds[i];
		kp.pt = charucoCorners[i];
		keypoints.push_back(kp);
	}

}
