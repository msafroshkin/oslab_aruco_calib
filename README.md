# OSLab_aruco_calib

##Introduction

This repository provides a multi-camera calibration tool. The calibration is done using opencv Aruco markers and CCalib library. The main source code can be seen under: <a href="https://gitlab.tuebingen.mpg.de/msafroshkin/oslab_aruco_calib/-/blob/master/ccalibTool/src/main.cpp">/ccalibTool/src directory</a>

The calibration tool supports only random pattern. For detail see <a href="https://gitlab.tuebingen.mpg.de/msafroshkin/oslab_aruco_calib/-/blob/master/ccalib/include/randpattern.hpp"><b>randomPattern.hpp</b></a>. Both pinhole and omnidirectional cameras are supported. For detail about omnidirectional camera see <a href="https://gitlab.tuebingen.mpg.de/msafroshkin/oslab_aruco_calib/-/blob/master/ccalib/src/include/omnidir.hpp"><b>omnidir.hpp</b></a>

##Usage

The multi-camera calibration tool can be through Command Prompt. It tooks one input:

* the json config file. It contains information about width and heigth of pattern, input filename (xml file), output filename, and the parameters that will be used during calibration process. An example config file is shown below. 

<img src= "doc/ConfigFile.PNG" />

<b>Input File (xml file)</b>

The xml file. contains the pattern filename, and the photo names.  photo names should be in form of cameraIdx-timestamp.*, and cameraIdx starts from 0.

An example xml file can be seen below. 

<img src= "doc/xml_file.PNG" />

An example python code to create xml can be found under<a href="https://gitlab.tuebingen.mpg.de/msafroshkin/oslab_aruco_calib/-/blob/master/tests/Test1/CreateXML.py"><b>CreateXML.py</b></a>

<b> pattern file</b>
<img src= "tests/Test1/pattern/ArucoMarkerBoard2.bmp" />


An example command line for calibration by using Aruco pattern: 

C:\\OSLab_CCALIB\\build\\Release>OSLab_Aruco_calib.exe -i path_to_ConfigFile

It returns a an output xml file that contains the camera parameters. An example of it can be seen under <a href="https://gitlab.tuebingen.mpg.de/msafroshkin/oslab_aruco_calib/-/blob/master/tests/Test1/Output_cmd.xml"><b>Output_cmd.xml</b></a>
