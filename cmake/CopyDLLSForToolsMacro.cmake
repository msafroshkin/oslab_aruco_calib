# Copy Dlls for all dependencies for debug and release configurations
macro(copyDllsForTools destDir)
   
    message(STATUS "Configuring files for directory ${destDir}")
    make_directory(${destDir}/Release)
	make_directory(${destDir}/Debug)
	
	# List all DDLs from all dependencies
    # Add here new dependencies... 	
	file(GLOB OPENCVDLLs_Debug ${OPENCV_DLL_DIR}/Debug/*d.dll)
	file(GLOB OPENCVDLLs_Release ${OPENCV_DLL_DIR}/Release/*.dll)
	
	# Copy DLLs to Debug and Release
	foreach(OPENCV_Dll_Debug ${OPENCVDLLs_Debug})
		file(COPY ${OPENCV_Dll_Debug} DESTINATION ${destDir}/Debug)
    endforeach(OPENCV_Dll_Debug)
	
	foreach(OPENCV_Dll_Release ${OPENCVDLLs_Release})
        file(COPY ${OPENCV_Dll_Release} DESTINATION ${destDir}/Release)
    endforeach(OPENCV_Dll_Release)
	
endmacro(copyDllsForTools)
