#ReDo:
macro (MarkIOI TOOL)
MESSAGE(STATUS Marking ${TOOL} executable)
MESSAGE(STATUS $ENV{IOI_DIR}\\bin\\export_core_module.exe ${CMAKE_CURRENT_BINARY_DIR}\\$(Configuration)\\${TOOL}.exe)
add_custom_command(TARGET ${TOOL}  POST_BUILD
                   COMMAND ($ENV{IOI_DIR}\\bin\\export_core_module.exe ${CMAKE_CURRENT_BINARY_DIR}\\${Configuration}\\${TOOL}.exe ))
endmacro(MarkIOI)