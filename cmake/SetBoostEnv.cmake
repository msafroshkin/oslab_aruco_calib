# Set Boost Enviroment in to Projects

# We support only Micrpsoft Visual Compiler 
if (MSVC) 
	set(ENV{BOOST_ROOT} $ENV{PREPARE_ENV_DIR}/Boost)
	if(CMAKE_SIZEOF_VOID_P EQUAL 8)
		set(Boost_LIBRARY_DIR $ENV{BOOST_ROOT}/stage64/lib)
	elseif(CMAKE_SIZEOF_VOID_P EQUAL 4)
		set(Boost_LIBRARY_DIR  $ENV{BOOST_ROOT}/stage32/lib)
	endif()
	set(Boost_USE_STATIC_LIBS ON)
    set(Boost_USE_MULTITHREADED ON)
else()
	# Linux goes here, Not Used!
	set(Boost_INCLUDE_DIR  $ENV{BOOST_ROOT}/include)
endif()

# Suport for multipale MSVC Versions
if (${MSVC_VERSION} EQUAL 1900)
	set(Boost_LIBRARY_DIR  $ENV{BOOST_ROOT}/stage64/lib/msvc140)
endif()
if ((${MSVC_VERSION} GREATER_EQUAL 1910) AND (${MSVC_VERSION} LESS 1920))
	set(Boost_LIBRARY_DIR  $ENV{BOOST_ROOT}/stage64/lib/msvc141)
endif()

# Adding Boost components libraries
MESSAGE(STATUS Looking for required components...)
# Add more find_package(...) here.
find_package(Boost 1.65 REQUIRED COMPONENTS program_options )
MESSAGE(STATUS foundBoostLibraries)
MESSAGE(STATUS ${Boost_LIBRARIES})

set(Boost_LIBRARIES ${Boost_LIBRARIES})
set(Boost_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
