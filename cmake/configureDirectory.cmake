macro (configure_directory srcDir destDir)
	message(STATUS "Configuring directory ${destDir}")
	if (NOT EXISTS "${destDir}")
		make_directory(${destDir})
	endif()
	file(GLOB  templateFiles ${srcDir})
    foreach(templateFile ${templateFiles})
        file(COPY ${templateFile} DESTINATION ${destDir})
    endforeach(templateFile)
endmacro(configure_directory)