import json

config = {}
config['patternWidth'] = 720
config['patternHeight'] = 540
config['outputFileName'] = "C:\\Users\\oslab\\Documents\\oslab_projects\\oslab_aruco_calib\\config\\Output_example1.xml"
config['inputFileName'] = "C:\\Users\\oslab\\Documents\\oslab_projects\\oslab_aruco_calib\\config\\Calib_example1.xml"
config['fix_K1'] = False
config['fix_K2'] = False
config['fix_K3'] = False
config['fix_K4'] = False
config['fix_K5'] = False
config['fix_K6'] = False
config['fix_S1_S2_S3_S4'] = False
config['fix_TangentialDistortion'] = False
config['rationalModel'] = False
config['fixRatio'] = False
config['fixPrincipalPoint'] = False
config['verbose'] = 0
config['showFeatureExtraction'] = False
config['numberOfCameras'] = 2
config['minimum_matches'] = 10


with open('calibrationConfig_example1.json', 'w') as f:
    f.write(json.dumps(config, indent = 4))
