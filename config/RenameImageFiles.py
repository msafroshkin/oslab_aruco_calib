import os

EXAMPLE_IMAGES_PATH = r"C:\Users\oslab\Documents\oslab_projects\calib_example\example1"
CAMERA_FOLDERS = os.listdir(EXAMPLE_IMAGES_PATH)

for camera in CAMERA_FOLDERS:
    camerano = camera.split("_")[2]
    if camerano == '03':
        new_camerano = '0'
    elif camerano == '04':
        new_camerano = '1'

    camera_path = os.path.join(EXAMPLE_IMAGES_PATH, camera)
    images_list = os.listdir(camera_path)
    for image in images_list:
        image_path = os.path.join(camera_path, image)
        frameno = image.split('_')[-1].replace('.png', '')
        new_image_name = new_camerano + "-" + frameno + '.png'
        new_image_path = os.path.join(camera_path, new_image_name)
        os.rename(image_path, new_image_path)



