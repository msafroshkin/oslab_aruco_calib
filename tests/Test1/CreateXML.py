import os
import glob
import xml.etree.ElementTree as ET

XML_TEMPLATE = "..\\..\\config\\calib_template.xml"
PATTERN_PATH = ".\\pattern\\ArucoMarkerBoard2.bmp"
IMAGES_ROOT = ".\\images"
OUTPUT_FILENAME = "Calib_example1_small.xml"

def list_files_under_root(root_path, ext = ''):
    """
    lists the all files under a given root
    :param root_path: the root path
    :param ext: extension of the requested file type. default is None
    :return: if ext parameter is None, lists all files under the given root
            else lists the files with the given extension under the given root
    """
    if ext.startswith('.'):
        raise TypeError('Please write extension without a dot, i.e., png')
    if ext:
        pattern = '/**/*.' + ext
    else :
        pattern = '/**'
    # list all files under all subdirectories
    file_list = [path for path in glob.glob(f'{root_path}{pattern}', recursive=True)
                 if os.path.isfile(path)]
    if file_list == []:
        print('Could not find any file. Please check root_path and extension')
    else:
        print("Files list was created successfully")
    return file_list

# save image names into a list
images_list = list_files_under_root(IMAGES_ROOT)
# add pattern path to the beginning of this list
xml_content_list = [PATTERN_PATH] + images_list

#add quotes
xml_content_list = ['"' + img + '"'  for img in xml_content_list]
# convert list to a string
xml_content = '\n'.join(xml_content_list)

# read template xml file
tree = ET.parse(XML_TEMPLATE)
root = tree.getroot()
#write xml content
root[0].text = "\n" + xml_content + "\n"


tree.write(OUTPUT_FILENAME, xml_declaration=True)