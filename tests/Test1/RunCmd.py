import os
from subprocess import run

EXE_PATH = "..\\..\\build\\ccalibTool\\Debug\\CCalib_Tool.exe "
CONFIG_NAME = ".\\calibrationConfig_example1_small_cmd.json"

code = EXE_PATH + "-i " + CONFIG_NAME
result = run(code, shell=True, capture_output=True, text=True)
print(result.returncode)
print(result.stdout)