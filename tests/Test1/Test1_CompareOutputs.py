import xml.etree.ElementTree as ET

OUTPUT1 = "Output_master.xml"
OUTPUT2 = "Output_cmd.xml"

# read OUTPUT1
tree1 = ET.parse(OUTPUT1)
root1 = tree1.getroot()

# read OUTPUT2
tree2 = ET.parse(OUTPUT1)
root2 = tree2.getroot()

if len(root1)!=len(root2):
    raise AssertionError('Number of items are not equal')
else:
    print("Number of items are equal")

for i in range(len(root1)):
    print(root1[i].tag)
    if root1[i].text==root2[i].text:
        print("Same")
    else:
        raise AssertionError("Content is different")

